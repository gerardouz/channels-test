from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json
from .models import persona
from channels.db import database_sync_to_async
import asyncio
from channels.consumer import AsyncConsumer

"""class ChatConsumer ( WebsocketConsumer ):
    def connect ( self ):
        self.room_name = self.scope[ 'url_route' ][ 'kwargs' ][ 'room_name' ]
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room group
        async_to_sync ( self.channel_layer.group_add )(
            self.room_group_name ,
            self.channel_name
        )

        self.accept()

    def disconnect ( self , close_code ):
        # Leave room group
        async_to_sync ( self.channel_layer.group_discard )(
            self.room_group_name ,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive ( self , text_data ):
        text_data_json = json.loads( text_data )
        message = text_data_json [ 'message' ]

        # Send message to room group
        async_to_sync ( self.channel_layer.group_send )(
            self.room_group_name ,
            {
                'type' : 'chat_message' ,
                'message' : message
            }
        )

    # Receive message from room group
    def chat_message ( self , event ):
        message = event [ 'message' ]

        # Send message to WebSocket
        self.send ( text_data = json.dumps ({
            'message' : message
        }))"""


class ChatConsumer(AsyncConsumer):

    async def websocket_connect(self, event):
        print("connected", event)

        await self.send({
            "type": "websocket.accept"
        })

        while True:
            await asyncio.sleep(2)

            obj = persona.objects.all().get(pk=1).nombre
            datos = {
                'type': 'websocket.send',
                'text':  obj,
            }
            await self.send(datos)
            x = json.dumps(datos)
            print (x)
            


    async def websocket_receive(self, event):
        print("receive", event)

       


    async def websocket_disconnect(self, event):
        print("disconnected", event)


"""
async def websocket_connect(self,event):
    print("connected",event) #here i am calling the function which returns the json array
    data=self.get_obj() await self.send({"type":"websocket.accept",
                                     "text":json.dumps(data) }) ) # this is the json data array i am sending to my template 
    #this function will return a json array when called 
    def get_obj(self): objects = modelname.objects.all() content={ 'objs':self.objs_to_json(objects) } 
        return content 
    def objs_to_json(self,objects): 
        #this is my json array result=[] 
        for objs in objects: 
        result.append(self.objects_to_json(objs)) 
    def objects_to_json(self,objs): 
        return { 'name':objs.name, 'date':objs.str(objs.date), } 
"""

"""
async def websocket_connect(self,event): 
    print("connected",event) #here i am calling the function which returns the json array
    data=self.get_obj() await self.send({ "type":"websocket.accept","text":json.dumps({'content':data}) }) ) # this is the json data array i am sending to my template 
#this function will return a json array when called 
def get_obj(self): objects = modelname.objects.all() result=[] 
for objs in objects: result.append({'name':objs.name,'thread':objs.thread}) 
return result 
 """